This repo is a fork of https://github.com/ros/rosdistro

This fork is created to allow installing native dependencies of gtm-robot packages using  `rosdep install <gtm-robot-packagename>` command.

The custom installation scripts are put in ./source_installers.
These scripts are then referenced in rosdep rules file in ./rosdep/base.yaml and ./rosdep/python.yaml

For -pip dependencies you need to have pip installed: `sudo apt-get install python-pip`

Usage:

To use this repository copy file ./10-gtm.list from this repo to your  /etc/ros/rosdep/sources.list.d/ directory.

After that update your dep index using the following command:
```bash
rosdep update
```

To test if everything works run the command:
```bash
rosdep resolve fadecandy
```
which should output:
```
#source
https://bitbucket.org/88088/gtm-robot-distro/raw/master/source_installers/fadecandy.rdmanifest
```

For -pip packages you need to 


More about using custom dependencies can be found in rosdep docs:
http://docs.ros.org/independent/api/rosdep/html/contributing_rules.html